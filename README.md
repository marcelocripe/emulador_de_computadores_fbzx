pt-BR:

O emulador de computadores FBZX foi criado por Sergio Costas Rodriguez (rastersoft@gmail.com). Emula o computador ZX Spectrum capaz de executar as edições 2 e 3 de 48 K, a edição original de 128 K, o Amstrad +2, o Amstrad +2A e o Spanish de 128 K.

Contém: um arquivo de texto em idioma "pt-BR" explicando como proceder a instalação do emulador e o ícone de atalho (arquivo .desktop) traduzido.

Baixe ou descarregue ou transfira o arquivo de texto ".txt" e o arquivo compactado ".zip".

Todos os créditos e direitos estão incluídos nos arquivos, em respeito ao trabalho voluntário de cada pessoa que participou e colaborou para que estes arquivos pudessem ser disponibilizados nesta página eletrônica.

marcelocripe

- - - - -

de:

Der Computeremulator FBZX wurde von Sergio Costas Rodriguez (rastersoft@gmail.com) erstellt. Emuliert den ZX Spectrum-Computer, der 48K Editionen 2 und 3, 128K Originalausgabe, Amstrad +2, Amstrad +2A und 128K Spanisch ausführen kann.

Enthält: eine Textdatei in "pt-BR"-Sprache, die erklärt, wie der Emulator installiert wird, und das übersetzte Verknüpfungssymbol (.desktop-Datei).

Laden Sie die Textdatei „.txt“ und die komprimierte Datei „.zip“ herunter.

Alle Credits und Rechte sind in den Dateien enthalten, in Bezug auf die freiwillige Arbeit jeder Person, die teilgenommen und mitgearbeitet hat, damit diese Dateien auf dieser Website verfügbar gemacht werden konnten.

marcelocripe

- - - - -

en:

The FBZX computer emulator was created by Sergio Costas Rodriguez (rastersoft@gmail.com). Emulates the ZX Spectrum computer capable of running 48K editions 2 and 3, 128K original edition, Amstrad +2, Amstrad +2A, and 128K Spanish.

Contains: a text file in "pt-BR" language explaining how to install the emulator and the translated shortcut icon (.desktop file).

Download the ".txt" text file and the ".zip" compressed file.

All credits and rights are included in the files, in respect for the voluntary work of each person who participated and collaborated so that these files could be made available on this website.

marcelocripe

- - - - -

es:

El emulador de computadora FBZX fue creado por Sergio Costas Rodriguez (rastersoft@gmail.com). Emula la computadora ZX Spectrum capaz de ejecutar 48K ediciones 2 y 3, 128K edición original, Amstrad +2, Amstrad +2A y 128K español.

Contiene: un archivo de texto en idioma "pt-BR" que explica cómo instalar el emulador y el icono de acceso directo traducido (archivo .desktop).

Descargue el archivo de texto ".txt" y el archivo comprimido ".zip".

Todos los créditos y derechos están incluidos en los archivos, en respeto al trabajo voluntario de cada persona que participó y colaboró ​​para que estos archivos pudieran estar disponibles en este sitio web.

marcelocripe

- - - - -

fr :

L'émulateur informatique FBZX a été créé par Sergio Costas Rodriguez (rastersoft@gmail.com). Émule l'ordinateur ZX Spectrum capable d'exécuter les éditions 48K 2 et 3, l'édition originale 128K, Amstrad +2, Amstrad +2A et 128K espagnol.

Contient : un fichier texte en langage "pt-BR" expliquant comment installer l'émulateur et l'icône de raccourci traduite (fichier .desktop).

Téléchargez le fichier texte ".txt" et le fichier compressé ".zip".

Tous les crédits et droits sont inclus dans les fichiers, dans le respect du travail bénévole de chaque personne qui a participé et collaboré afin que ces fichiers puissent être mis à disposition sur ce site.

marcelocripe

- - - - -

it:

L'emulatore di computer FBZX è stato creato da Sergio Costas Rodriguez (rastersoft@gmail.com). Emula il computer ZX Spectrum in grado di eseguire 48K edizioni 2 e 3, 128K edizione originale, Amstrad +2, Amstrad +2A e 128K spagnolo.

Contiene: un file di testo in lingua "pt-BR" che spiega come installare l'emulatore e l'icona del collegamento tradotto (file .desktop).

Scarica il file di testo ".txt" e il file compresso ".zip".

Tutti i crediti ei diritti sono inclusi nei file, nel rispetto del lavoro volontario di ogni persona che ha partecipato e collaborato affinché questi file potessero essere resi disponibili su questo sito web.

marcelocripe
